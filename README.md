# Laila Social Network Bot :robot:
Laila is a bot created with selenium for posting tweets,
and another texts in social networks based on "scrapped" news.

The principal advantage of Laila is your "APIless" philosophy,
so at the start we don't have the limitations for post or request information
of APIs.
![Laila Twetter](laila.jpg)

