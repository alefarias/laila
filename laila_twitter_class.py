from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
import time, re

class LailaTwitter():

    def __init__(self):
        path_driver = 'C:\\webdrivers\\chromedriver.exe'
        self.driver = webdriver.Chrome(path_driver)
        self.driver.get('https://twitter.com/login')
        self.email = ''
        self.password = ''
        self.logged = False

    def get_element(self, field, value):
        try:
            element = WebDriverWait(self.driver, 30).until(EC.element_to_be_clickable((field, value)))
        finally:
            return self.driver.find_element(field, value)
        
    def login(self):
        email = self.get_element(By.CLASS_NAME, 'js-username-field')
        password = self.get_element(By.CLASS_NAME, 'js-password-field')
        email.send_keys(self.email)
        time.sleep(2)
        password.send_keys(self.password)
        time.sleep(2)
        password.send_keys(Keys.ENTER)
        self.logged = True
        
    def post(self, message):
        box_timeline = self.get_element(By.ID, 'tweet-box-home-timeline')
        try:
            box_timeline.send_keys(message)
            box_timeline.send_keys(Keys.CONTROL, Keys.ENTER)
        except:
            time.sleep(2)
            self.post(message)

    def close(self):
        self.driver.close()

corpus = open('deadpoll.txt', 'r', encoding = 'utf-8')
corpus_text = corpus.read()
corpus.close()
corpus_text = re.sub('\n+', '\n', corpus_text)
corpus_text = corpus_text.split('\n')

laila = LailaTwitter()
laila.login()
print('logged')

n = len(corpus_text) - 1
for message in reversed(corpus_text):
    time.sleep(2)
    laila.post(str(n)+': '+message[:122].strip()+' #deadpoolscript')
    print('post:'+message[:129].strip()+' #deadpoolscript')
    n -= 1
laila.close()


